$(document).ready(function(){
    //iterate through each textboxes and add keyup
//handler to trigger sum event
$("input").each(function() {

$(this).keyup(function(){
calculateSum();
});
});

});

function calculateSum() {

var sum = 0;
//iterate through each textboxes and add the values
$("input").each(function() {

//add only if the value is number
if(!isNaN(this.value) && this.value.length!=0) {
sum += parseFloat(this.value);
}

});
//.toFixed() method will roundoff the final sum to 2 decimal places
$("#res").html(sum.toFixed(2));
}